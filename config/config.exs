# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :mpstats,
  # We don't include Mpstats.Cache.Repo here because we don't want to
  # ever migrate it.
  ecto_repos: [Mpstats.Repo],
  generators: [timestamp_type: :utc_datetime]

# Job scheduler
config :mpstats, Oban,
  repo: Mpstats.Repo,
  plugins: [
    # Clear completed jobs after a week
    {Oban.Plugins.Pruner, max_age: 60 * 60 * 24 * 7},
    # If a job got orphaned by a system restart, retry after 30min
    {Oban.Plugins.Lifeline, rescue_after: :timer.minutes(30)},
    # https://hexdocs.pm/oban/Oban.html#module-periodic-jobs
    {Oban.Plugins.Cron,
     crontab: [
       # {"* * * * *", Mpstats.DefaultWorker, args: %{"task" => "heartbeat"}, max_attempts: 1},
       {"33 1 * * *", Mpstats.DefaultWorker, args: %{"task" => "load_recent_parliament_data"}}
     ]}
  ],
  queues: [default: 10]

# Configures the endpoint
config :mpstats, MpstatsWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Phoenix.Endpoint.Cowboy2Adapter,
  render_errors: [
    formats: [html: MpstatsWeb.ErrorHTML, json: MpstatsWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: Mpstats.PubSub,
  live_view: [signing_salt: "+0mKp5Mh"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :mpstats, Mpstats.Mailer, adapter: Swoosh.Adapters.Local

# Configure Hammer rate-limiting
config :hammer,
  backend: {Hammer.Backend.ETS, [expiry_ms: 60_000 * 60, cleanup_interval_ms: 60_000 * 10]}

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.17.11",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.3.2",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
