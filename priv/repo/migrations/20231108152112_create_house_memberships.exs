defmodule Mpstats.Repo.Migrations.CreateHouseMemberships do
  use Ecto.Migration

  def change do
    create table(:house_memberships) do
      add :house, :text, null: false
      add :start_date, :date, null: false
      add :end_date, :date
      add :membership_from, :text, null: false
      add :member_id, references(:members, on_delete: :nothing), null: false

      timestamps(type: :utc_datetime)
    end

    create index(:house_memberships, [:member_id])
    create index(:house_memberships, [:member_id, :house, :start_date], unique: true)
  end
end
