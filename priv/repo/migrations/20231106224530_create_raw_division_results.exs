defmodule Mpstats.Repo.Migrations.CreateRawDivisionResults do
  use Ecto.Migration

  def change do
    create table(:raw_division_results) do
      add :vote, :text
      add :name, :text
      add :member_from, :text
      add :member_id, :integer
      add :party, :text
      add :date, :date
      add :proxy_name, :text

      timestamps(type: :utc_datetime)
    end
  end
end
