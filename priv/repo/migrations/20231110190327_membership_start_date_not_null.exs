defmodule Mpstats.Repo.Migrations.MembershipStartDateNotNull do
  use Ecto.Migration

  def change do
    alter table(:house_memberships) do
      modify :start_date, :date, null: false
    end
  end
end
