defmodule Mpstats.Repo.Migrations.CreatePartyMemberships do
  use Ecto.Migration

  def change do
    create table(:parties) do
      add :name, :text, null: false
      add :abbrev, :text, null: false
      add :color, :text, null: false

      timestamps(type: :utc_datetime)
    end

    create table(:party_memberships) do
      add :start_date, :date, null: false
      add :end_date, :date
      add :party_name, :text, null: false
      add :is_independent_party, :boolean, default: false, null: false
      add :member_id, references(:members, on_delete: :nothing), null: false
      add :party_id, references(:parties, on_delete: :nothing), null: false

      timestamps(type: :utc_datetime)
    end

    create index(:party_memberships, [:member_id])
    create index(:party_memberships, [:party_id])
  end
end
