defmodule Mpstats.Repo.Migrations.CreateRawDivisions do
  use Ecto.Migration

  def change do
    create table(:raw_divisions) do
      add :date, :date
      add :datetime, :naive_datetime
      add :title, :text
      add :aye_count, :integer
      add :no_count, :integer
      add :number, :integer

      timestamps(type: :utc_datetime)
    end
  end
end
