defmodule Mpstats.Repo.Migrations.RevampDivisions2 do
  use Ecto.Migration

  def change do
    alter table(:division_results) do
      add :division_id, references(:divisions, on_delete: :nothing), null: false
      add :title, :text, null: false
    end

    create index(:division_results, [:title])
    create index(:division_results, [:division_id])
  end
end
