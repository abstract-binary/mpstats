defmodule Mpstats.Repo.Migrations.AddDivisionIdAndName do
  use Ecto.Migration

  def change do
    alter table(:raw_division_results) do
      add :division_id, :integer
      add :title, :text
    end
  end
end
