defmodule Mpstats.Repo.Migrations.AddMemberCurrentMembershipFrom do
  use Ecto.Migration

  def change do
    alter table(:members) do
      add :current_membership_from, :text
    end
  end
end
