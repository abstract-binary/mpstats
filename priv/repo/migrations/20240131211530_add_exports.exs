defmodule Mpstats.Repo.Migrations.AddExports do
  use Ecto.Migration

  def change do
    create table(:exports) do
      add :name, :text, null: false
      add :date, :date, null: false
      add :generated, :utc_datetime, null: false
      add :url, :text, null: false

      timestamps(type: :utc_datetime)
    end

    create index(:exports, [:name, :date], unique: true)
  end
end
