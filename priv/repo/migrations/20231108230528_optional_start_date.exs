defmodule Mpstats.Repo.Migrations.OptionalStartDate do
  use Ecto.Migration

  def change do
    alter table(:house_memberships) do
      modify :start_date, :date, null: true
    end
  end
end
