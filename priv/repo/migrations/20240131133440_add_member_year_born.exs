defmodule Mpstats.Repo.Migrations.AddMemberYearBorn do
  use Ecto.Migration

  def change do
    alter table(:members) do
      add :year_born, :integer
    end
  end
end
