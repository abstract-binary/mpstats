defmodule Mpstats.Repo.Migrations.AddMemberAgeWebsiteWiki do
  use Ecto.Migration

  def change do
    alter table(:members) do
      add :dob, :date
      add :website, :text
      add :wikipedia, :text
    end
  end
end
