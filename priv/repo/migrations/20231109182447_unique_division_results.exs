defmodule Mpstats.Repo.Migrations.UniqueDivisionResults do
  use Ecto.Migration

  def change do
    create index(:division_results, [:member_id, :division_id], unique: true)
  end
end
