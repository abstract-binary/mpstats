defmodule Mpstats.Repo.Migrations.AddMemberDobYearBornConstraint do
  use Ecto.Migration

  def change do
    create constraint(:members, :dob_and_year_born_match, check: "dob IS NULL OR (year_born IS NOT NULL AND year_born = date_part('year', dob))")
  end
end
