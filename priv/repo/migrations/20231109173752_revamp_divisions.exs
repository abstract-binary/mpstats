defmodule Mpstats.Repo.Migrations.RevampDivisions do
  use Ecto.Migration

  def change do
    drop table(:raw_divisions)

    create table(:divisions) do
      add :date, :date, null: false
      add :datetime, :naive_datetime, null: false
      add :title, :text, null: false
      add :aye_count, :integer, null: false
      add :no_count, :integer, null: false
      add :number, :integer, null: false

      timestamps(type: :utc_datetime)
    end

    create index(:divisions, [:date])
    create index(:divisions, [:datetime])
    create index(:divisions, [:title])
    create index(:divisions, [:number])

    drop table(:raw_division_results)

    create table(:division_results) do
      add :vote, :text, null: false
      add :name, :text, null: false
      add :member_from, :text, null: false
      add :member_id, references(:members, on_delete: :nothing), null: false
      add :party, :text, null: false
      add :date, :date, null: false
      add :proxy_name, :text

      timestamps(type: :utc_datetime)
    end

    create index(:division_results, [:name])
    create index(:division_results, [:member_from])
    create index(:division_results, [:member_id])
    create index(:division_results, [:party])
    create index(:division_results, [:date])
    create index(:division_results, [:proxy_name])
  end
end
