defmodule Mpstats.Repo.Migrations.CreateMembers do
  use Ecto.Migration

  def change do
    create table(:members) do
      add :name, :text

      timestamps(type: :utc_datetime)
    end
  end
end
