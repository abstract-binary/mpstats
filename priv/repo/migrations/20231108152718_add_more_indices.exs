defmodule Mpstats.Repo.Migrations.AddMoreIndices do
  use Ecto.Migration

  def change do
    create index(:party_memberships, [:member_id, :party_id, :start_date], unique: true)
    create index(:parties, [:name])
    create index(:members, [:name])
  end
end
