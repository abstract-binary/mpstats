defmodule Mpstats.Repo.Migrations.AddCurrentMemberStatus do
  use Ecto.Migration

  def change do
    alter table(:members) do
      add :active, :bool, null: false, default: false
      add :current_house, :text
      add :current_party, references(:parties, on_delete: :nothing), null: true
    end
  end
end
