defmodule Mpstats.Repo.Migrations.AddMemberThumbnail do
  use Ecto.Migration

  def change do
    alter table(:members) do
      add :thumbnail_upstream_url, :text
      add :thumbnail_url, :text
    end
  end
end
