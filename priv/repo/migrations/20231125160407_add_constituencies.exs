defmodule Mpstats.Repo.Migrations.AddConstituencies do
  use Ecto.Migration

  def change do
    create table(:constituencies) do
      add :name, :text, null: false

      timestamps(type: :utc_datetime)
    end

    alter table(:house_memberships) do
      add :membership_from_id, references(:constituencies, on_delete: :nothing)
    end

    create index(:constituencies, [:name])
  end
end
