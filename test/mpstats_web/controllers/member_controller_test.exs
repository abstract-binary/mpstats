defmodule MpstatsWeb.MemberControllerTest do
  use MpstatsWeb.ConnCase

  import Mpstats.MembersFixtures

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  describe "index" do
    test "lists all member", %{conn: conn} do
      conn = get(conn, ~p"/member")
      assert html_response(conn, 200) =~ "Listing Member"
    end
  end

  describe "new member" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/member/new")
      assert html_response(conn, 200) =~ "New Member"
    end
  end

  describe "create member" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/member", member: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/member/#{id}"

      conn = get(conn, ~p"/member/#{id}")
      assert html_response(conn, 200) =~ "Member #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/member", member: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Member"
    end
  end

  describe "edit member" do
    setup [:create_member]

    test "renders form for editing chosen member", %{conn: conn, member: member} do
      conn = get(conn, ~p"/member/#{member}/edit")
      assert html_response(conn, 200) =~ "Edit Member"
    end
  end

  describe "update member" do
    setup [:create_member]

    test "redirects when data is valid", %{conn: conn, member: member} do
      conn = put(conn, ~p"/member/#{member}", member: @update_attrs)
      assert redirected_to(conn) == ~p"/member/#{member}"

      conn = get(conn, ~p"/member/#{member}")
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, member: member} do
      conn = put(conn, ~p"/member/#{member}", member: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Member"
    end
  end

  describe "delete member" do
    setup [:create_member]

    test "deletes chosen member", %{conn: conn, member: member} do
      conn = delete(conn, ~p"/member/#{member}")
      assert redirected_to(conn) == ~p"/member"

      assert_error_sent 404, fn ->
        get(conn, ~p"/member/#{member}")
      end
    end
  end

  defp create_member(_) do
    member = member_fixture()
    %{member: member}
  end
end
