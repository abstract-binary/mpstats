# MPStats

> Numerically quantifying MP activity

## Status

**Under Development**

## Live

https://mpstats.fly.dev/

## Local development

To start your Phoenix server:

  * Setup a Postgres database running on localhost named
    `mpstats_dev`,
  * Run `mix setup` to install and setup dependencies,
  * Configure the necessary env variables,
  * Start a dev shell with the server running with `just dev`.

Now you can visit [`localhost:4000`](http://localhost:4000) from your
browser.

## Environment variables

Required in prod and dev:
- `AWS_REGION`: S3 region (e.g. `nl-ams`),
- `AWS_HOST`: S3 host (e.g. `s3.nl-ams.scw.cloud`),
- `AWS_ACCESS_KEY_ID`: S3 access key id,
- `AWS_SECRET_ACCESS_KEY`: S3 secret access key,
- `S3_BUCKET`: S3 bucket id

Required only in prod:
- `SECRET_KEY_BASE`: Phoenix secret salt,
- `DATABASE_URL`: URL to connect to.
- `ADMIN_USERNAME`: username for dashboard,
- `ADMIN_PASSWORD`: password for dashboard,
