defmodule Mpstats.Export do
  use Ecto.Schema
  import Ecto.Changeset

  schema "exports" do
    field :name, :string
    field :date, :date
    field :generated, :utc_datetime
    field :url, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(export, attrs) do
    export
    |> cast(attrs, [
      :id,
      :name,
      :date,
      :generated,
      :url
    ])
    |> validate_required([:name, :date, :generated, :url])
    |> unique_constraint(:id, name: "exports_pkey")
    |> unique_constraint([:name, :date], name: "exports_name_date_index")
  end
end
