defmodule Mpstats.Cache.Member do
  use Ecto.Schema
  import Ecto.Changeset

  schema "members" do
    field :name, :string
    field :active, :boolean
    field :current_house, :string
    field :current_membership_from, :string
    field :thumbnail_url, :string

    belongs_to :party, Mpstats.Cache.Party, foreign_key: :current_party
    # How many years the person has served for thus far
    field :service_age, :integer

    # How old the person is.  This could be exact is `dob` is known,
    # or approximate if only `year_born` is known, or nil if neither
    # is known.
    field :age, :integer

    field :inserted_at, :utc_datetime
  end

  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, [
      :id,
      :name,
      :active,
      :current_house,
      :current_house,
      :current_membership_from,
      :thumbnail_url,
      :current_party,
      :service_age,
      :age,
      :inserted_at
    ])
    |> validate_required([:id, :name, :active, :inserted_at, :service_age])
    |> unique_constraint(:id, name: "primary_key")
  end
end
