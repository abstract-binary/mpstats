defmodule Mpstats.Cache.Party do
  use Ecto.Schema
  import Ecto.Changeset

  schema "parties" do
    field :abbrev, :string
    field :color, :string
    field :name, :string
    field :inserted_at, :utc_datetime
  end

  @doc false
  def changeset(party, attrs) do
    party
    |> cast(attrs, [:id, :name, :abbrev, :color, :inserted_at])
    |> validate_required([:id, :name, :abbrev, :color, :inserted_at])
    |> unique_constraint(:id, name: "primary_key")
  end
end
