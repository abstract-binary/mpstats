defmodule Mpstats.Problems do
  @moduledoc """
  Find problems in the data and provide HEEx fragments for fixing
  them.
  """

  @checks %{
    missing_dob: {Mpstats.Problems.MissingDOB, :list}
  }

  def list_problems() do
    @checks
    |> Enum.flat_map(fn {_name, {mod, fun}} ->
      apply(mod, fun, [])
      |> Enum.map(fn params ->
        {mod, params}
      end)
    end)
  end
end
