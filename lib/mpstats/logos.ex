defmodule Mpstats.Logos do
  @labour_logo File.read!("lib/logos/labour.svg")
  @tory_logo File.read!("lib/logos/tory.svg")
  @snp_logo File.read!("lib/logos/snp.svg")
  @libdem_logo File.read!("lib/logos/libdem.svg")
  @commons_logo File.read!("lib/logos/commons.svg")
  @lords_logo File.read!("lib/logos/lords.svg")

  def svg(abbrev, height, opts \\ [])

  def svg("Con", height, _opts), do: EEx.eval_string(@tory_logo, height: height)
  def svg("Lab", height, _opts), do: EEx.eval_string(@labour_logo, height: height)
  def svg("SNP", height, _opts), do: EEx.eval_string(@snp_logo, height: height)
  def svg("LD", height, _opts), do: EEx.eval_string(@libdem_logo, height: height)
  def svg("commons", height, _opts), do: EEx.eval_string(@commons_logo, height: height)
  def svg("lords", height, _opts), do: EEx.eval_string(@lords_logo, height: height)

  def svg(abbrev, height, opts) when is_binary(abbrev) do
    fallback = Keyword.get(opts, :fallback, "")

    """
      <svg viewBox="0 0 500 100" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="<%= height %>">
        <rect fill="#eee" x="0" y="0" width="500" height="100" />
        <text x="80" y="70" font-size="4rem"><%= fallback %></text>
      </svg>
    """
    |> EEx.eval_string(fallback: fallback, height: height)
  end
end
