defmodule Mpstats.Party do
  use Ecto.Schema
  import Ecto.Changeset

  schema "parties" do
    field :abbrev, :string
    field :color, :string
    field :name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(party, attrs) do
    party
    |> cast(attrs, [:id, :name, :abbrev, :color])
    |> validate_required([:id, :name, :abbrev, :color])
    |> unique_constraint(:id, name: "parties_pkey")
  end
end
