defmodule Mpstats.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    :ok = Oban.Telemetry.attach_default_logger()

    children = [
      MpstatsWeb.Telemetry,
      {Task.Supervisor, name: Mpstats.Tasks},
      Mpstats.Repo,
      Mpstats.Cache.Repo,
      Mpstats.Cache,
      {DNSCluster, query: Application.get_env(:mpstats, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Mpstats.PubSub},
      {Finch, name: Mpstats.Finch},
      {Oban, Application.fetch_env!(:mpstats, Oban)},
      Mpstats.S3,

      # Start to serve requests, typically the last entry
      MpstatsWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Mpstats.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    MpstatsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
