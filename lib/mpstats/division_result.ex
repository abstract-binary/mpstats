defmodule Mpstats.DivisionResult do
  use Ecto.Schema
  import Ecto.Changeset

  # Beware that the party and constituency of the vote might be
  # different from the *current* party of the MP.  This record is
  # meant to represent the *latest* information.
  schema "division_results" do
    field :date, :date
    field :member_from, :string
    field :name, :string
    field :party, :string
    field :vote, :string
    field :proxy_name, :string
    field :title, :string

    belongs_to :member, Mpstats.Member
    belongs_to :division, Mpstats.Division

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(division_result, attrs) do
    division_result
    |> cast(attrs, [
      :vote,
      :name,
      :member_from,
      :member_id,
      :party,
      :date,
      :proxy_name,
      :division_id,
      :title
    ])
    |> validate_required([
      :vote,
      :name,
      :member_from,
      :member_id,
      :party,
      :date,
      :division_id,
      :title
    ])
    |> foreign_key_constraint(:member_id)
    |> foreign_key_constraint(:division_id)
  end
end
