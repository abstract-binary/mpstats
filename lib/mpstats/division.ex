defmodule Mpstats.Division do
  use Ecto.Schema
  import Ecto.Changeset

  schema "divisions" do
    field :aye_count, :integer
    field :date, :date
    field :datetime, :naive_datetime
    field :no_count, :integer
    field :number, :integer
    field :title, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(division, attrs) do
    division
    |> cast(attrs, [:id, :date, :datetime, :title, :aye_count, :no_count, :number])
    |> validate_required([:id, :date, :datetime, :title, :aye_count, :no_count, :number])
    |> unique_constraint(:id, name: "divisions_pkey")
  end
end
