defmodule Mpstats.ParliamentAPI do
  @moduledoc """
  Download data from the Parliament developer portal and load it into
  the database.

  https://developer.parliament.uk/
  """

  import Ecto.Query

  require Logger

  alias Mpstats.HouseMembership
  alias Mpstats.Member
  alias Mpstats.Party
  alias Mpstats.Constituency
  alias Mpstats.PartyMembership
  alias Mpstats.Division
  alias Mpstats.DivisionResult

  @houses [:commons, :lords]

  # TODO A lot of the `on_conflict` `Repo.inserts` should actually be
  # `Repo.update`.  This matters because the latter updates
  # `updated_at` but the former doesn't.

  @doc """
  Fetch and load all the constituencies.
  """
  def load_constituencies() do
    fetch_constituencies()
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:id]
    )
  end

  @doc """
  Fetch all the constituencies.
  """
  def fetch_constituencies() do
    # Download this many results at once
    results_in_batch = 20

    num_constituencies =
      api_get!(
        "https://members-api.parliament.uk/api/Location/Constituency/Search?skip=0&take=#{results_in_batch}"
      )
      |> then(fn x -> x["totalResults"] end)

    Enum.flat_map(0..(ceil(num_constituencies / results_in_batch) - 1), fn idx ->
      api_get!(
        "https://members-api.parliament.uk/api/Location/Constituency/Search?skip=#{idx * results_in_batch}&take=#{results_in_batch}"
      )
      |> then(fn x -> x["items"] end)
      |> Enum.map(fn x ->
        Constituency.changeset(%Constituency{}, %{
          id: x["value"]["id"],
          name: x["value"]["name"]
        })
      end)
    end)
  end

  @doc """
  Load thumbnails for members that don't have them yet.  These are
  uploaded to S3.
  """
  def load_new_members_thumbnails(_opts \\ []) do
    members_wo_thumbnails =
      from(m in Mpstats.Member,
        where: not is_nil(m.thumbnail_upstream_url) and is_nil(m.thumbnail_url)
      )
      |> Mpstats.Repo.all()

    for m <- members_wo_thumbnails do
      %{id: id, thumbnail_upstream_url: thumbnail_upstream_url} = m
      {content, content_type} = api_get_any!(thumbnail_upstream_url)

      case content_type do
        "image/tiff" ->
          # I don't want to think about this
          :ok

        _ ->
          {:ok, thumbnail_url} = Mpstats.S3.upload_thumbnail(id, content, content_type)

          %Member{} =
            Member.changeset(m, %{thumbnail_url: thumbnail_url})
            |> Mpstats.Repo.insert!(
              on_conflict: {:replace, [:thumbnail_url]},
              conflict_target: [:id]
            )
      end
    end

    :ok
  end

  @doc """
  Load histories for members.

  ## Options:

  * `:members` - The members to load.  Defaults to all.
  """
  def load_all_members_histories(opts \\ []) do
    members =
      Keyword.get_lazy(opts, :members, fn ->
        Member |> Mpstats.Repo.all() |> Enum.map(fn x -> x.id end)
      end)

    for member_id <- members do
      case(load_member_history(member_id)) do
        :ok -> :ok
        {:error, error} -> raise "Failed to load member history for #{member_id}: #{error}"
      end
    end

    :ok
  end

  @doc """
  Fetch and load the history of a single member.
  """
  def load_member_history(member_id) when is_integer(member_id) do
    load_member_history(Mpstats.Repo.one(from m in Member, where: m.id == ^member_id))
  end

  def load_member_history(%Member{} = member) do
    Logger.info("Loading member history for #{member.name} (#{member.id})")

    with(
      {:party, party_memberships, :house, house_memberships} <-
        fetch_member_history(member.id),
      :ok <-
        party_memberships
        |> insert_one_by_one(
          on_conflict: {:replace_all_except, [:id, :inserted_at]},
          conflict_target: [:member_id, :party_id, :start_date]
        ),
      :ok <-
        house_memberships
        |> insert_one_by_one(
          on_conflict: {:replace_all_except, [:id, :inserted_at]},
          conflict_target: [:member_id, :house, :start_date]
        )
    ) do
      :ok
    end
  end

  @doc """
  Fetch the history of a single member.
  """
  def fetch_member_history(member_id) when is_integer(member_id) do
    # The `latestHouseMembership` and `houseMembershipHistory` values
    # are inconsistent, so try to handle this assuming that `latest*`
    # has the better data.  I sent softwareengineering@ but I'm not
    # optimistic that this will be fixed.
    latest_membership =
      api_get!("https://members-api.parliament.uk/api/Members/#{member_id}")
      |> then(fn x -> x["value"]["latestHouseMembership"] end)
      |> then(fn json ->
        start_date =
          if json["membershipStatus"] && json["membershipStatus"]["statusStartDate"] do
            json["membershipStatus"]["statusStartDate"]
            |> NaiveDateTime.from_iso8601!()
            |> NaiveDateTime.to_date()
          else
            json["membershipStartDate"] &&
              json["membershipStartDate"]
              |> NaiveDateTime.from_iso8601!()
              |> NaiveDateTime.to_date()
          end

        end_date =
          json["membershipEndDate"] &&
            json["membershipEndDate"]
            |> NaiveDateTime.from_iso8601!()
            |> NaiveDateTime.to_date()

        membership_from = json["membershipFrom"]

        if start_date != nil and end_date != nil and membership_from != nil do
          [
            HouseMembership.changeset(%HouseMembership{}, %{
              member_id: member_id,
              start_date: start_date,
              end_date: end_date,
              house:
                case membership_from do
                  # A bunch of lords have a null house
                  "Hereditary" -> "lords"
                  "Life peer" -> "lords"
                  "Excepted Hereditary" -> "lords"
                  _ -> to_string(house_atom(json["house"]))
                end,
              membership_from: membership_from
            })
          ]
        else
          []
        end
      end)

    json =
      api_get!("https://members-api.parliament.uk/api/Members/History?ids=#{member_id}")
      |> List.first()
      |> then(fn x -> x["value"] end)

    party_memberships =
      json["partyHistory"]
      |> Enum.map(fn x ->
        PartyMembership.changeset(%PartyMembership{}, %{
          member_id: member_id,
          end_date:
            x["endDate"] &&
              x["endDate"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date(),
          start_date: x["startDate"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date(),
          party_id: x["party"]["id"],
          party_name: x["party"]["name"],
          is_independent_party: x["party"]["isIndependentParty"]
        })
      end)

    valid_constituencies =
      from(c in Constituency, select: c.id) |> Mpstats.Repo.all() |> Enum.into(MapSet.new())

    house_memberships =
      json["houseMembershipHistory"]
      |> Enum.flat_map(fn x ->
        start_date =
          x["membershipStartDate"] &&
            x["membershipStartDate"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date()

        end_date =
          x["membershipEndDate"] &&
            x["membershipEndDate"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date()

        if start_date != nil or end_date != nil do
          membership_from_id = x["membershipFromId"]

          [
            HouseMembership.changeset(%HouseMembership{}, %{
              member_id: member_id,
              start_date: start_date,
              end_date: end_date,
              house:
                case x["membershipFrom"] do
                  # A bunch of lords have a null house
                  "Hereditary" -> "lords"
                  "Life peer" -> "lords"
                  "Excepted Hereditary" -> "lords"
                  _ -> to_string(house_atom(x["house"]))
                end,
              membership_from: x["membershipFrom"],
              # Some of the older members reference constituencies
              # that aren't returned by a constituency search.
              membership_from_id:
                if(membership_from_id in valid_constituencies, do: membership_from_id, else: nil)
            })
          ]
        else
          []
        end
      end)

    {:party, party_memberships, :house, house_memberships ++ latest_membership}
  end

  @doc """
  Fetch and load all members' contact information.
  """
  def load_all_members_contacts() do
    from(m in Member, where: m.active)
    |> Mpstats.Repo.all()
    |> Enum.map(&fetch_member_contact(&1))
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:id]
    )
  end

  @doc """
  Fetch a member's contact information.
  """
  def fetch_member_contact(member_id) when is_integer(member_id) do
    from(m in Member, where: m.id == ^member_id)
    |> Mpstats.Repo.one()
    |> fetch_member_contact()
  end

  def fetch_member_contact(%Member{} = member) do
    entries =
      api_get!("https://members-api.parliament.uk/api/Members/#{member.id}/Contact")
      |> then(fn x -> x["value"] end)

    website =
      entries
      |> Enum.find(&(&1["type"] == "Website"))
      |> then(& &1["line1"])

    Member.changeset(member, %{website: website})
  end

  @doc """
  Fetch and load the members in the `:commons` or `:lords`.  It is
  sufficient to run this once for today's date to get all the members
  who have ever been.
  """
  def load_members(house, %Date{} = date) when house in @houses do
    fetch_members(house, date)
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:id]
    )
  end

  def fetch_members(house, %Date{} = date) when house in @houses do
    num_members =
      api_get!(
        "https://members-api.parliament.uk/api/Members/Search?House=#{house_num(house)}&skip=0&take=20&MembershipInDateRange.WasMemberOnOrBefore=#{date}"
      )
      |> then(fn x -> x["totalResults"] end)

    old_members =
      for m <- Member |> Mpstats.Repo.all(), into: %{} do
        {m.id, m}
      end

    # Download this many results at once
    results_in_batch = 20

    Enum.flat_map(0..(ceil(num_members / results_in_batch) - 1), fn idx ->
      api_get!(
        "https://members-api.parliament.uk/api/Members/Search?House=#{house_num(house)}&skip=#{idx * results_in_batch}&take=#{results_in_batch}&MembershipInDateRange.WasMemberOnOrBefore=#{date}"
      )
      |> then(fn x -> x["items"] end)
      |> Enum.map(fn x ->
        x = x["value"]
        id = x["id"]

        Member.changeset(Map.get(old_members, id, %Member{}), %{
          id: id,
          name: x["nameDisplayAs"],
          active:
            get_in(x, ["latestHouseMembership", "membershipStatus", "statusIsActive"]) || false,
          current_house: get_in(x, ["latestHouseMembership", "house"]) |> house_atom |> to_string,
          current_party: get_in(x, ["latestParty", "id"]),
          current_membership_from: get_in(x, ["latestHouseMembership", "membershipFrom"]),
          thumbnail_upstream_url: x["thumbnailUrl"]
        })
      end)
    end)
  end

  @doc """
  Load all the parties by querying for a few select dates.

  ## Options:

  * `:start_date` - The date from which to start querying.  Defaults
    to 1990-01-01.

  * `:end_date` - The date from which to start querying.  Defaults to
    today.
  """
  def load_all_parties(opts \\ []) do
    start_date = Keyword.get(opts, :start_date, ~D[1990-01-01])
    end_date = Keyword.get(opts, :end_date, Date.utc_today())

    for date <- Date.range(start_date, end_date, 30) do
      for house <- [:commons, :lords] do
        case load_parties(house, date) do
          :ok -> :ok
          {:error, error} -> raise "Failed to load parties for #{house} and #{date}: #{error}"
        end
      end
    end

    :ok
  end

  @doc """
  Fetch and load the parties in the `:commons` or `:lords`.
  """
  def load_parties(house, %Date{} = date) when house in @houses do
    fetch_parties(house, date)
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:id]
    )
  end

  @doc """
  Fetch the parties in the `:commons` or `:lords`.
  """
  def fetch_parties(house, %Date{} = date) when house in @houses do
    parties =
      api_get!(
        "https://members-api.parliament.uk/api/Parties/StateOfTheParties/#{house_num(house)}/#{date}"
      )
      |> then(fn x -> x["items"] end)
      |> Enum.map(fn x ->
        x = x["value"]["party"]

        Party.changeset(%Party{}, %{
          id: x["id"],
          color: normalize_color(x["backgroundColour"] || x["foregroundColour"] || "#8D1B83"),
          name: x["name"],
          abbrev: x["abbreviation"] || x["name"]
        })
      end)

    parties
  end

  @doc """
  Fetch and load all divisions (there aren't that many of them).

  ## Options:

  * `:start_date` - The date from which to start querying.  Defaults
    to 2016-03-09 which is the first date in the Parliament API.

  * `:end_date` - The date from which to start querying.  Defaults to
    today.
  """
  def load_all_divisions(opts \\ []) do
    start_date = Keyword.get(opts, :start_date, ~D[2016-03-09])
    end_date = Keyword.get(opts, :end_date, Date.utc_today())
    load_divisions(start_date, end_date)
  end

  @doc """
  Fetch and load divisions.
  """
  def load_divisions(%Date{} = start_date, %Date{} = end_date) do
    fetch_divisions(start_date, end_date)
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:id]
    )
  end

  @doc """
  Fetch divisions between the given dates.
  """
  def fetch_divisions(%Date{} = start_date, %Date{} = end_date) do
    num_results =
      api_get!(
        "https://commonsvotes-api.parliament.uk/data/divisions.json/searchTotalResults?queryParameters.startDate=#{start_date}&queryParameters.endDate=#{end_date}"
      )

    if not is_integer(num_results) do
      raise "Got something other than a number when searching for total results: '#{num_results}'"
    end

    # Download this many results at once
    results_in_batch = 25

    res =
      Enum.flat_map(0..(ceil(num_results / results_in_batch) - 1), fn idx ->
        api_get!(
          "https://commonsvotes-api.parliament.uk/data/divisions.json/search?queryParameters.startDate=#{start_date}&queryParameters.endDate=#{end_date}&queryParameters.skip=#{idx * results_in_batch}&queryParameters.take=#{results_in_batch}"
        )
        |> Enum.map(fn x ->
          Division.changeset(
            %Division{},
            %{
              id: x["DivisionId"],
              title: x["Title"],
              date: x["Date"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date(),
              datetime: x["Date"] |> NaiveDateTime.from_iso8601!(),
              aye_count: x["AyeCount"],
              no_count: x["NoCount"],
              number: x["Number"]
            }
          )
        end)
      end)

    if length(res) != num_results do
      raise "Expected to get #{num_results} results, but got #{length(res)}"
    end

    res
  end

  @doc """
  Fetch and load division results.
  """
  def load_division_results(division_id) when is_integer(division_id) do
    fetch_division_result(division_id)
    |> insert_one_by_one(
      on_conflict: {:replace_all_except, [:id, :inserted_at]},
      conflict_target: [:member_id, :division_id]
    )
  end

  @doc """
  Fetch the division result and return it as a list of
  `DivisionResult` changesets.
  """
  def fetch_division_result(division_id) when is_integer(division_id) do
    json =
      api_get!("https://commonsvotes-api.parliament.uk/data/division/#{division_id}.json")

    date = json["Date"] |> NaiveDateTime.from_iso8601!() |> NaiveDateTime.to_date()
    json_division_id = json["DivisionId"]
    title = json["Title"]

    if division_id != json_division_id do
      raise "Downloaded division_id (#{json_division_id}) doesn't match requested one (#{division_id})"
    end

    Stream.concat([
      Stream.concat(json["Ayes"], json["AyeTellers"] || [])
      |> Stream.map(fn x -> x |> downcase_keys() |> Map.put("vote", "aye") end),
      Stream.concat(json["Noes"], json["NoTellers"] || [])
      |> Stream.map(fn x -> x |> downcase_keys() |> Map.put("vote", "no") end),
      json["NoVoteRecorded"]
      |> Stream.map(fn x -> x |> downcase_keys() |> Map.put("vote", "no_vote_recorded") end)
    ])
    |> Stream.map(fn x ->
      x |> Map.put("date", date) |> Map.put("title", title) |> Map.put("division_id", division_id)
    end)
    |> Enum.map(fn x -> DivisionResult.changeset(%DivisionResult{}, x) end)
  end

  # Rename the keys of a map from "SomethingLikeThis" to
  # "something_like_this".
  defp downcase_keys(map) when is_map(map) do
    map
    |> Enum.map(fn {key, value} -> {Macro.underscore(key), value} end)
    |> Map.new()
  end

  # Insert given changesets into the repo one by one.
  defp insert_one_by_one(changesets, opts)
  defp insert_one_by_one([], _opts), do: :ok

  defp insert_one_by_one(changesets, opts) do
    changesets
    |> Enum.map(fn c -> Mpstats.Repo.insert(c, opts) end)
    |> Enum.group_by(fn {res, _} -> res end)
    |> case do
      %{error: errors} -> {:error, errors}
      %{ok: _} -> :ok
    end
  end

  # Convert between the names and numbers for the two houses.

  defp house_num(:commons), do: 1
  defp house_num(:lords), do: 2

  defp house_atom(1), do: :commons
  defp house_atom(2), do: :lords

  # Do an API request again the Parliament website.
  #
  # All responses are expected to be JSON.
  #
  # Requests are rate-limited to 4 per second to make sure we don't trip
  # some limiting on the Parliament side.  This function will block
  # until the request has a chance to run.

  defp api_get!(url) when is_binary(url) do
    api_get_internal!(url, 3)
    |> then(fn x -> x.body end)
    |> Jason.decode!()
  end

  # Returns `{binary_content, content_type}`.  E.g. `{<<66, 77, ..>>,
  # "image/jpeg"}`.
  defp api_get_any!(url) when is_binary(url) do
    resp = api_get_internal!(url, 3)
    {"content-type", content_type} = List.keyfind(resp.headers, "content-type", 0)
    {resp.body, content_type}
  end

  defp api_get_internal!(url, retries_left) when retries_left <= 0 do
    raise("Ran out of retries trying to get #{url}")
  end

  defp api_get_internal!(url, retries_left) when is_binary(url) and is_integer(retries_left) do
    case Hammer.check_rate("ParliamentAPI", 1_000, 5) do
      {:allow, _} ->
        Logger.info("GET #{url}")

        Finch.build(
          "GET",
          url,
          [{"Accept", "application/json"}]
        )
        |> Finch.request(Mpstats.Finch, pool_timeout: 30_000, receive_timeout: 60_000)
        |> case do
          {:ok, resp} ->
            resp

          {:error, error} ->
            Logger.error("Failed to download GET #{url}: #{error}")
            Process.sleep(50 + :rand.uniform(100))
            api_get_internal!(url, retries_left - 1)
        end

      {:deny, _} ->
        Logger.info("Rate-limited: GET #{url}")
        Process.sleep(50 + :rand.uniform(100))
        api_get_internal!(url, retries_left)
    end
  end

  # Ensure a color code starts with a hash.
  def normalize_color(<<?#, _::binary-size(6)>> = color), do: color
  def normalize_color(<<?#, _::binary-size(3)>> = color), do: color
  def normalize_color(<<_::binary-size(6)>> = color), do: <<?#, color::binary>>
  def normalize_color(<<_::binary-size(3)>> = color), do: <<?#, color::binary>>
end
