defmodule Mpstats.Cache do
  @moduledoc """
  High-level interface to the in-memory cache.

  The idea behind this cache is that most data in MPStats doesn't
  change frequently (it's usually only changed by the midnight refresh
  job).  Also, there are going to be a few queries that are repeated
  very frequently such as listing parties or listing members.  Let's
  store cache these results in memory so that we don't have to
  round-trip to the database on a different machine (and possibly on a
  different rack).

  The `get_*` functions hit the cache directly and don't go through
  the `GenServer`.

  Note that many of the submodules have names identical to the ones in
  the larger app.  The submodule types are smaller, denormalized, and
  hold only the information needed by the common case.
  """

  use GenServer

  import Ecto.Query

  alias Mpstats.Cache

  def start_link(opts) when is_list(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @doc """
  Request that the cache be refreshed.  Blocks until completion.
  """
  def refresh_cache() do
    GenServer.call(__MODULE__, :refresh_cache, :infinity)
  end

  @doc """
  Get all cached parties.  Returns a map from party id to party.
  """
  def get_parties() do
    from(p in Cache.Party, select: {p.id, p})
    |> Cache.Repo.all()
    |> Enum.map(fn {k, p} -> {k, Map.from_struct(p)} end)
    |> Enum.into(%{})
  end

  @doc """
  Get all cached members.
  """
  def get_members() do
    Cache.Member
    |> Ecto.Query.preload([:party])
    |> Cache.Repo.all()
  end

  # Server (callbacks)

  @impl true
  def init(_opts) do
    case refresh_cache_internal() do
      :ok -> {:ok, %{}}
      {:error, err} -> {:error, err}
    end
  end

  @impl true
  def handle_call(:refresh_cache, _from, state) do
    res = refresh_cache_internal()
    {:reply, res, state}
  end

  # TODO Add table constraint that house is in ["lords", "commons"]

  # TODO Service age needs to be computed separately for commons and
  # lords.  See 1467 (David Cameron).

  # Update all cache tables with values from the database.
  defp refresh_cache_internal() do
    Task.Supervisor.async(Mpstats.Tasks, fn ->
      try do
        :ok = precache_gen(db_mod: Mpstats.Party, cache_mod: Cache.Party)

        :ok =
          precache_gen(
            db_mod: Mpstats.Member,
            preload: [:house_memberships],
            cache_mod: Cache.Member,
            convert: fn old, x ->
              x =
                x
                |> Map.put(:service_age, service_age(x))
                |> Map.put(:age, age(x))
              Cache.Member.changeset(old, x)
            end
          )
      rescue
        err ->
          {:error, err}
      catch
        err ->
          {:error, err}
      end
    end)
    |> Task.await(:infinity)
  end

  @doc false
  # Update the cache with values from the database.
  #
  # ## Options:
  #
  # `:db_mod` - The name of the database module.
  # `:preload` - List of associations to preload.  Defaults to `[]`.
  # `:cache_mod` - The name of the cache module.
  # `:convert` - A function that converts from a DB value to a cache
  #    value.  Called with the old or fresh cache value and the DB value;
  #    should return a cache changeset.  Defaults to just
  #    `CacheMod.changeset`.
  def precache_gen(opts) do
    db_mod = Keyword.get(opts, :db_mod)
    preload_opt = Keyword.get(opts, :preload, [])
    cache_mod = Keyword.get(opts, :cache_mod)

    convert =
      Keyword.get(opts, :convert, fn old, x ->
        apply(cache_mod, :changeset, [old, x])
      end)

    now = DateTime.utc_now() |> DateTime.truncate(:second)
    db_vals = db_mod |> preload(^preload_opt) |> Mpstats.Repo.all()

    for db_val <- db_vals do
      db_val = db_val |> Map.from_struct() |> Map.put(:inserted_at, now)

      case from(p in cache_mod, where: p.id == ^db_val.id) |> Cache.Repo.one() do
        nil ->
          convert.(struct(cache_mod), db_val)
          |> Cache.Repo.insert!()

        %{} = cache_val ->
          convert.(cache_val, db_val)
          |> Cache.Repo.update!()
      end
    end

    db_parties_ids =
      for db_val <- db_vals, into: MapSet.new() do
        db_val.id
      end

    for cache_val <- cache_mod |> Cache.Repo.all(),
        cache_val.id not in db_parties_ids do
      Cache.Repo.delete!(cache_val)
    end

    :ok
  end

  # Compute the length of time a member has served in Parliament.
  defp service_age(%{} = m) do
    for x <- m.house_memberships do
      case x.end_date do
        nil -> Date.diff(Date.utc_today(), x.start_date)
        end_date -> Date.diff(end_date, x.start_date)
      end
    end
    |> Enum.sum()
    |> then(fn x -> ceil(x / 365.0) end)
  end

  # Compute the age of a member or an approximation.
  defp age(%{} = m) do
    today = Date.utc_today()
    case m.dob do
      %Date{} = dob ->
        floor(Date.diff(today, dob) / 365.0)

      nil ->
        case m.year_born do
          y when is_integer(y) ->
            today.year - y
          nil -> nil
        end
    end
  end
end
