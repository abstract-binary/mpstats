defmodule Mpstats.Constituency do
  use Ecto.Schema
  import Ecto.Changeset

  schema "constituencies" do
    field :name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(party, attrs) do
    party
    |> cast(attrs, [:id, :name])
    |> validate_required([:id, :name])
  end
end
