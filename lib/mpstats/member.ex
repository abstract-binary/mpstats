defmodule Mpstats.Member do
  use Ecto.Schema
  import Ecto.Changeset

  schema "members" do
    field :name, :string
    field :active, :boolean
    field :current_house, :string
    field :current_membership_from, :string
    field :thumbnail_upstream_url, :string
    field :thumbnail_url, :string
    field :dob, :date
    field :website, :string
    field :wikipedia, :string
    field :year_born, :integer

    belongs_to :party, Mpstats.Party, foreign_key: :current_party
    has_many :party_memberships, Mpstats.PartyMembership

    has_many :house_memberships, Mpstats.HouseMembership

    has_many :division_results, Mpstats.DivisionResult

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, [
      :id,
      :name,
      :active,
      :current_house,
      :current_party,
      :current_membership_from,
      :thumbnail_upstream_url,
      :thumbnail_url,
      :dob,
      :website,
      :wikipedia,
      :year_born
    ])
    |> validate_required([:id, :name, :active])
    |> unique_constraint(:id, name: "members_pkey")
    |> foreign_key_constraint(:current_party)
    |> check_constraint(:dob, name: "dob_and_year_born_match")
    |> check_constraint(:year_born, name: "dob_and_year_born_match")
  end
end
