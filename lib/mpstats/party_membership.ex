defmodule Mpstats.PartyMembership do
  use Ecto.Schema
  import Ecto.Changeset

  schema "party_memberships" do
    field :end_date, :date
    field :is_independent_party, :boolean, default: false
    field :party_name, :string
    field :start_date, :date

    belongs_to :member, Mpstats.Member
    belongs_to :party, Mpstats.Party

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(party_membership, attrs) do
    party_membership
    |> cast(attrs, [
      :start_date,
      :end_date,
      :party_name,
      :is_independent_party,
      :member_id,
      :party_id
    ])
    |> validate_required([
      :start_date,
      :party_name,
      :is_independent_party,
      :member_id,
      :party_id
    ])
    |> foreign_key_constraint(:member_id)
    |> foreign_key_constraint(:party_id)
  end
end
