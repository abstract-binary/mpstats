defmodule Mpstats.S3 do
  @moduledoc """
  Helpers for interacting with S3 storage for static assets.

  This is a `GenServer`, partially so that we have a place to store
  the configuration (so as to not re-fetch it from env vars on every
  request), and partially to serialize uploads to S3 (because S3
  behaves weirdly on occasion and serialized uploads are easier to
  reason about).
  """

  use GenServer

  require Logger

  @request_timeout 65_000

  def start_link(opts) when is_list(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def upload_thumbnail(member_id, contents, "image/jpeg")
      when is_integer(member_id) and is_binary(contents) do
    GenServer.call(
      __MODULE__,
      {:upload_thumbnail, "#{member_id}.jpg", contents},
      @request_timeout
    )
  end

  def upload_thumbnail(member_id, contents, "image/png")
      when is_integer(member_id) and is_binary(contents) do
    GenServer.call(
      __MODULE__,
      {:upload_thumbnail, "#{member_id}.png", contents},
      @request_timeout
    )
  end

  def upload_export_csv(%Date{} = date, name, %Explorer.DataFrame{} = contents)
      when is_binary(name) do
    GenServer.call(__MODULE__, {:upload_export_csv, date, name, contents}, @request_timeout)
  end

  @impl true
  def init(_opts) do
    {:system, host_env} = Application.get_env(:ex_aws, :s3)[:host]

    {:ok,
     %{
       s3_bucket: Application.get_env(:mpstats, :s3_bucket),
       s3_scheme: Application.get_env(:ex_aws, :s3)[:scheme],
       s3_host: System.fetch_env!(host_env)
     }}
  end

  @impl true
  def handle_call(
        {:upload_thumbnail, thumbnail_name, contents},
        _from,
        %{s3_bucket: s3_bucket} = state
      ) do
    object_name = "members/thumbnails/#{thumbnail_name}"

    Logger.info("Upload to S3: #{object_name}")

    res =
      with(
        {:ok, _} <-
          ExAws.S3.put_object(s3_bucket, object_name, contents)
          |> ExAws.request(),
        {:ok, _} <-
          ExAws.S3.put_object_acl(s3_bucket, object_name, [{:acl, :public_read}])
          |> ExAws.request()
      ) do
        {:ok, url(object_name, state)}
      else
        {:error, error} -> {:error, error}
      end

    {:reply, res, state}
  end

  def handle_call(
        {:upload_export_csv, date, name, contents},
        _from,
        %{s3_bucket: s3_bucket} = state
      ) do
    object_name = "export/#{date}/#{name}"

    Logger.info("Upload to S3: #{object_name}")

    res =
      with(
        {:ok, _} <-
          ExAws.S3.put_object(s3_bucket, object_name, contents |> Explorer.DataFrame.dump_csv!())
          |> ExAws.request(),
        {:ok, _} <-
          ExAws.S3.put_object_acl(s3_bucket, object_name, [{:acl, :public_read}])
          |> ExAws.request()
      ) do
        {:ok, url(object_name, state)}
      else
        {:error, error} -> {:error, error}
      end

    {:reply, res, state}
  end

  defp url(path, %{s3_bucket: s3_bucket, s3_scheme: s3_scheme, s3_host: s3_host}) do
    "#{s3_scheme}#{s3_bucket}.#{s3_host}/#{path}"
  end
end
