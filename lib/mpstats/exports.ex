defmodule Mpstats.Exports do
  @moduledoc """
  Functions for exporting data and uploading it to S3.
  """

  import Ecto.Query, warn: false

  alias Mpstats.Member
  alias Mpstats.Export
  alias Mpstats.Repo
  alias Explorer.DataFrame, as: DF

  @doc """
  List all exports grouped by name.

  ## Options:

  * `:limit` - The number of exports to keep for each name.  Defaults to 5.
  """
  def list_exports(opts \\ []) do
    limit = Keyword.get(opts, :limit, 5)

    Export
    |> Repo.all()
    |> Enum.group_by(& &1.name)
    |> Enum.map(fn {name, group} ->
      group =
        group
        |> Enum.sort_by(& &1.generated, {:desc, DateTime})
        |> Enum.take(limit)

      {name, group}
    end)
    |> Enum.into(%{})
  end

  @doc """
  Export members as they currently are to a CSV and upload it to S3 while recording the
  URL in the `exports` table.
  """
  def export_members() do
    members_csv =
      from(m in Member, preload: [:party])
      |> Repo.all()
      |> Enum.map(fn m ->
        [
          id: m.id,
          name: m.name,
          active: m.active,
          house: m.current_house,
          membership_from: m.current_membership_from,
          thumbnail_url: m.thumbnail_url,
          date_of_birth: m.dob,
          year_born: m.year_born,
          website: m.website,
          wikipedia: m.wikipedia,
          party: m.party && m.party.name,
          updated_at: m.updated_at |> DateTime.to_iso8601()
        ]
      end)
      |> DF.new()

    today = Date.utc_today()
    {:ok, url} = Mpstats.S3.upload_export_csv(today, "members.csv", members_csv)

    {:ok, _} =
      Export.changeset(%Export{}, %{
        name: "members.csv",
        date: today,
        generated: DateTime.utc_now(),
        url: url
      })
      |> Repo.insert(
        on_conflict: {:replace, [:url]},
        conflict_target: [:name, :date]
      )

    :ok
  end
end
