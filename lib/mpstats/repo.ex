defmodule Mpstats.Repo do
  use Ecto.Repo,
    otp_app: :mpstats,
    adapter: Ecto.Adapters.Postgres
end
