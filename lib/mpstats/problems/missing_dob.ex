defmodule Mpstats.Problems.MissingDOB do
  use MpstatsWeb, :live_component

  import Ecto.Query, warn: false

  alias Mpstats.Member
  alias Mpstats.Repo

  def list() do
    from(m in Member, where: m.active == true and is_nil(m.dob) and is_nil(m.year_born))
    |> Repo.all()
    |> Enum.map(fn member ->
      %{member: member, id: "#missing-dob-#{member.id}"}
    end)
  end

  @impl true
  def mount(socket) do
    {:ok,
     socket
     |> assign(status: nil)
     |> assign(status_class: [])}
  end

  @impl true
  def render(assigns) do
    member = assigns.params.member

    search_terms =
      Enum.concat(
        member.name |> String.split(),
        member.current_membership_from |> String.split()
      )
      |> Enum.join("+")

    assigns =
      assigns
      |> assign(member: member)
      |> assign(search_url: "https://kagi.com/search?q=#{search_terms}")

    ~H"""
    <div class="mt-4 flex items-center gap-x-4">
      <div class="font-bold text-lg">
        Missing DOB:
      </div>
      <div>
        <.form for={%{}} as={:dob} phx-change="set-dob" phx-target={@myself}>
          <input type="date" name="dob" />
        </.form>
      </div>
      <div>
        <.form for={%{}} as={:dob} phx-submit="set-year-born" phx-target={@myself}>
          <input type="text" class="w-16" name="year-born" placeholder="1990" />
        </.form>
      </div>
      <div>
        <.form for={%{}} as={:dob} phx-submit="set-dob-text" phx-target={@myself}>
          <input type="text" name="dob-text" placeholder="1 January 1990" />
        </.form>
      </div>
      <div>
        <a href={~p"/members/#{@member}"} class="text-blue-700 underline"><%= @member.name %></a>
      </div>
      <div><%= @member.current_house %></div>
      <div><%= @member.current_membership_from %></div>
      <%= if @member.website do %>
        <div><a href={@member.website} class="text-blue-700 underline">Website</a></div>
      <% end %>
      <div><a href={@search_url} class="text-blue-700 underline">Kagi</a></div>
      <div>
        <span class={@status_class}><%= @status %></span>
      </div>
    </div>
    """
  end

  @impl true
  def handle_event("set-dob", %{"dob" => dob}, socket) do
    {:noreply, set_dob(dob, socket)}
  end

  @impl true
  def handle_event("set-dob-text", %{"dob-text" => dob_text}, socket) do
    case Timex.parse(dob_text, "%e %B %Y", :strftime) do
      {:ok, d} ->
        {:noreply, set_dob(NaiveDateTime.to_date(d), socket)}

      {:error, error} ->
        {:noreply, set_status(socket, :error, error)}
    end
  end

  def handle_event("set-year-born", %{"year-born" => year_born}, socket) do
    case Integer.parse(String.trim(year_born)) do
      {year_born, ""} ->
        {:noreply, set_year_born(year_born, socket)}

      :error ->
        set_status(socket, :error, "Invalid year")

      {_, trailing} ->
        set_status(socket, :error, "Trailing characters after year: '#{trailing}'")
    end
  end

  defp set_dob(dob = %Date{}, socket) do
    Member.changeset(socket.assigns.params.member, %{dob: dob, year_born: dob.year})
    |> Repo.update()
    |> case do
      {:ok, _} -> set_status(socket, :info, "DOB set")
      {:error, %{errors: errors}} -> set_status(socket, :error, inspect(errors))
    end
  end

  defp set_year_born(year_born, socket) when is_integer(year_born) do
    Member.changeset(socket.assigns.params.member, %{dob: nil, year_born: year_born})
    |> Repo.update()
    |> case do
      {:ok, _} -> set_status(socket, :info, "Year born set")
      {:error, %{errors: errors}} -> set_status(socket, :error, inspect(errors))
    end
  end

  defp set_status(socket, kind, text) when is_binary(text) and kind in [:info, :error] do
    socket
    |> assign(status: text)
    |> assign(
      status_class:
        if(kind == :info, do: ["text-green-700 font-bold"], else: ["text-red-700 font-bold"])
    )
  end
end
