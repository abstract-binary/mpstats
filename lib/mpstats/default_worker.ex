defmodule Mpstats.DefaultWorker do
  use Oban.Worker,
    queue: :default,
    max_attempts: 1,
    # https://hexdocs.pm/oban/Oban.html#module-unique-jobs
    unique: [
      period: 60 * 60 * 6,
      fields: [:queue, :args],
      # everything except for the final states: :completed,
      # :cancelled, and :discarded
      states: [:available, :scheduled, :executing, :retryable]
    ]

  import Ecto.Query

  require Logger

  alias Mpstats.ParliamentAPI

  def schedule_load_recent_parliament_data() do
    new(%{"task" => "load_recent_parliament_data"}) |> Oban.insert()
  end

  def schedule_load_new_members_thumbnails() do
    new(%{"task" => "load_new_members_thumbnails"}) |> Oban.insert()
  end

  def schedule_refresh_cache() do
    new(%{"task" => "refresh_cache"}) |> Oban.insert()
  end

  def schedule_daily_export() do
    new(%{"task" => "daily_export"}) |> Oban.insert()
  end

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"task" => "heartbeat"}}) do
    Logger.info("Still alive")
  end

  def perform(%Oban.Job{args: %{"task" => "load_recent_parliament_data"}}) do
    Logger.info("Loading recent parliament data: starting")
    today = Date.utc_today()
    :ok = ParliamentAPI.load_parties(:commons, today)
    :ok = ParliamentAPI.load_constituencies()
    :ok = ParliamentAPI.load_members(:commons, today)
    :ok = ParliamentAPI.load_parties(:lords, today)
    :ok = ParliamentAPI.load_members(:lords, today)

    active_members =
      from(m in Mpstats.Member, where: m.active)
      |> Mpstats.Repo.all()
      |> Enum.uniq_by(fn m -> m.id end)

    for member <- active_members do
      :ok = ParliamentAPI.load_member_history(member)
    end

    :ok = ParliamentAPI.load_all_members_contacts()

    one_month_ago = Date.add(today, -30)
    :ok = ParliamentAPI.load_divisions(one_month_ago, today)

    recent_divisions =
      from(d in Mpstats.Division, where: d.date >= ^one_month_ago, select: d.id)
      |> Mpstats.Repo.all()

    for division_id <- recent_divisions do
      :ok = ParliamentAPI.load_division_results(division_id)
    end

    Logger.info("Loading recent parliament data: complete")
    schedule_load_new_members_thumbnails()
    schedule_refresh_cache()
    schedule_daily_export()
    :ok
  end

  def perform(%Oban.Job{args: %{"task" => "load_new_members_thumbnails"}}) do
    Logger.info("Loading new members thumbnails: starting")
    :ok = ParliamentAPI.load_new_members_thumbnails()
    Logger.info("Loading new members thumbnails: complete")
    :ok
  end

  def perform(%Oban.Job{args: %{"task" => "refresh_cache"}}) do
    Logger.info("Refreshing cache: starting")
    :ok = Mpstats.Cache.refresh_cache()
    Logger.info("Refreshing cache: complete")
    :ok
  end

  def perform(%Oban.Job{args: %{"task" => "daily_export"}}) do
    Logger.info("Daily export: starting")
    :ok = Mpstats.Exports.export_members()
    Logger.info("Daily export: complete")
    :ok
  end
end
