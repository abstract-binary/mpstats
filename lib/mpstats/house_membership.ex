defmodule Mpstats.HouseMembership do
  use Ecto.Schema
  import Ecto.Changeset

  schema "house_memberships" do
    field :end_date, :date
    field :house, :string
    field :membership_from, :string
    field :start_date, :date

    belongs_to :member, Mpstats.Member
    belongs_to :constituency, Mpstats.Constituency, foreign_key: :membership_from_id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(house_membership, attrs) do
    house_membership
    |> cast(attrs, [
      :house,
      :start_date,
      :end_date,
      :membership_from,
      :membership_from_id,
      :member_id
    ])
    |> validate_required([:house, :start_date, :membership_from, :member_id])
    |> foreign_key_constraint(:member_id)
    |> foreign_key_constraint(:membership_from_id)
  end
end
