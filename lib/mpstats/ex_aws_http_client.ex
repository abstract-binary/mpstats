defmodule Mpstats.ExAwsHttpClient do
  @behaviour ExAws.Request.HttpClient

  require Logger

  def request(method, url, body, headers, http_opts) do
    case http_opts do
      [] -> :noop
      opts -> Logger.debug(inspect({:http_opts, opts}))
    end

    with {:ok, resp} <- http_request(method, url, headers, body) do
      {:ok, %{status_code: resp.status, body: resp.body, headers: resp.headers}}
    else
      {:error, reason} ->
        {:error, %{reason: reason}}
    end
  end

  defp http_request(method, url, headers, body) do
    http_request(Finch.build(method, url, headers, body), 3)
  end

  defp http_request(%{} = req, retries) when retries <= 0 do
    {:error, "Ran out of retries: #{req.path}"}
  end

  defp http_request(%{} = req, retries) do
    req
    |> Finch.request(Mpstats.Finch, pool_timeout: 10_000, receive_timeout: 10_000)
    |> case do
      {:ok, resp} ->
        {:ok, resp}

      {:error, error} ->
        Logger.info("Retrying upload: #{req.path} (#{inspect(error)})")
        Process.sleep(50 + :rand.uniform(100))
        http_request(req, retries - 1)
    end
  end
end
