defmodule Mpstats.Members do
  @moduledoc """
  The Members context.
  """

  import Ecto.Query, warn: false

  alias Explorer.DataFrame
  alias Mpstats.Repo
  alias Mpstats.Member

  @doc """
  Returns the list of members.

  ## Options

  * `:active` - Whether to load only active members. Defaults to true.

  ## Examples

      iex> list_member()
      [%Member{}, ...]

  """
  def list_members(opts \\ []) do
    active = Keyword.get(opts, :active, true)
    from(m in Member, where: m.active == ^active, preload: [:party]) |> Repo.all()
  end

  @doc """
  Gets a single member.

  Raises if the Member does not exist.

  ## Options

  * `:preload` - Which associations to preload.  Defaults to `[:party,
    :party_memberships, :house_memberships]`.

  ## Examples

      iex> get_member!(123)
      %Member{}

  """
  def get_member!(id, opts \\ []) when is_integer(id) or is_binary(id) do
    preload =
      Keyword.get(opts, :preload, []) ++
        [:party, :party_memberships, :house_memberships, {:party_memberships, :party}]

    case from(m in Member,
           where: m.id == ^id,
           preload: ^preload
         )
         |> Repo.one() do
      nil -> raise "Unknown member #{id}"
      %Member{} = m -> m
    end
  end

  @doc """
  Get the voting results for a member, as a `DataFrame` of `[:date,
  :vote]`.
  """
  def get_member_vote_results!(id, _opts \\ []) when is_integer(id) or is_binary(id) do
    from(r in Mpstats.DivisionResult,
      where: r.member_id == ^id,
      select: %{date: r.date, vote: r.vote}
    )
    |> Mpstats.Repo.all()
    |> case do
      [] -> nil
      x -> DataFrame.new(x)
    end
  end
end
