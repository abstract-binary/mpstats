defmodule MpstatsWeb.ProblemsLive.Index do
  use MpstatsWeb, :live_view

  alias Mpstats.Problems

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :problems, Problems.list_problems())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Problems")
  end

  # TODO Use or delete
  @impl true
  def handle_info({MpstatsWeb.ProblemLive.FormComponent, {:saved, problem}}, socket) do
    {:noreply, stream_insert(socket, :problems, problem)}
  end

  # TODO Use or delete
  @impl true
  def handle_event("delete", %{"id" => _id}, socket) do
    {:noreply, socket}
  end
end
