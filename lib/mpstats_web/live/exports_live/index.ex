defmodule MpstatsWeb.ExportsLive.Index do
  use MpstatsWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    exports = Mpstats.Exports.list_exports()

    {:ok,
     socket
     |> assign(:exports, exports)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Data Export")
  end
end
