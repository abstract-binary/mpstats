<.header>
  <%= if @member.thumbnail_url do %>
    <img class="h-12 w-12 flex-none rounded-full bg-gray-50" src={@member.thumbnail_url} />
  <% else %>
    <img class="h-12 w-12 flex-none rounded-full bg-gray-50" />
  <% end %>
  <%= @member.name %>
  <:subtitle><%= @member.current_membership_from %></:subtitle>
  <:breadcrumb href={~p"/"}>Home</:breadcrumb>
  <:breadcrumb href={~p"/members"}>MP Directory</:breadcrumb>
  <:breadcrumb href={~p"/members/#{@member.id}"}><%= @member.name %></:breadcrumb>
</.header>

<%= if @votes_cast do %>
  <.heading level={2} ref="graph">Graph</.heading>

  <.live_component module={MpstatsWeb.Live.VotesCastGraph} id="votes-cast-graph" data={@votes_cast} />
<% end %>

<.heading level={2} ref="info">Info</.heading>

<.list>
  <:item title="Name"><%= @member.name %></:item>
  <:item :if={@member.dob} title="Date of birth"><%= @member.dob %></:item>
  <:item title="Active"><%= @member.active %></:item>
  <:item title="House"><%= @member.current_house %></:item>
  <:item title="Party">
    <.logo
      :if={@member.party}
      abbrev={@member.party.abbrev}
      alt={@member.party.name}
      height={30}
    />
  </:item>
  <:item title="Parliament Profile"><a href={parliament_link(@member)} class="underline text-blue-700">Link</a></:item>
</.list>

<.heading level={2} ref="party-history">Party History</.heading>

<ul :for={m <- Enum.sort_by(@member.party_memberships, & &1.start_date, {:desc, Date})} role="list" class="divide-y divide-gray-100">
  <li class="flex justify-between gap-x-6 py-5">
    <div class="flex min-w-0 gap-x-4">
      <.logo abbrev={m.party.abbrev} alt={m.party.name} height={30} />
    </div>
    <div class="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
      <p class="text-sm leading-6 text-gray-900">
        <%= if !m.end_date do %>
        since
        <% end %>
        <time datetime={m.start_date}><%= m.start_date.year %></time>
        <%= if m.end_date do %>
          →
          <time datetime={m.end_date}><%= m.end_date.year %></time>
        <% end %>
      </p>
    </div>
  </li>
</ul>

<.heading level={2} ref="membership-history">Membership History</.heading>

<ul :for={m <- Enum.sort_by(@member.house_memberships, & &1.start_date, {:desc, Date})} role="list" class="divide-y divide-gray-100">
  <li class="flex justify-between gap-x-6 py-5">
    <div class="flex min-w-0 gap-x-4">
      <.logo abbrev={m.house} alt="House of Commons" height={48} class="rounded overflow-clip" />
      <div class="min-w-0 flex-auto">
        <p class="mt-1 truncate text-lg leading-5"><%= m.membership_from %></p>
      </div>
    </div>
    <div class="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
      <p class="text-sm leading-6 text-gray-900">
        <%= if !m.end_date do %>
        since
        <% end %>
        <time datetime={m.start_date}><%= m.start_date.year %></time>
        <%= if m.end_date do %>
          →
          <time datetime={m.end_date}><%= m.end_date.year %></time>
        <% end %>
      </p>
    </div>
  </li>
</ul>
