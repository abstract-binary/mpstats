defmodule MpstatsWeb.MemberLive.Filter do
  use Ecto.Schema
  import Ecto.Changeset

  @default_parties %{
    "tory" => true,
    "labour" => true,
    "libdem" => true,
    "snp" => true,
    "other" => true
  }

  @party_mapping %{
    "Conservative" => "tory",
    "Labour" => "labour",
    "Liberal Democrat" => "libdem",
    "Scottish National Party" => "snp"
  }

  @primary_key false
  embedded_schema do
    field :active, :string, default: "active"
    field :house, :string, default: "commons"
    field :name_constituency, :string, default: ""
    field :parties, :map, default: @default_parties
    field :limit, :integer, default: 10
    field :skip, :integer, default: 0
  end

  @doc false
  def changeset(filter, attrs) do
    filter
    |> cast(attrs, [:active, :house, :name_constituency, :parties, :limit, :skip])
  end

  @doc """
  Apply the filter to the list of members returning only those members
  that match.

  This is done on 'list of members' rather than 'individual members'
  in order to pre-compile some bits of the filter for speed.
  """
  def apply(filter, members) when is_list(members) do
    active_f =
      case filter.active do
        "all" -> fn _ -> true end
        "active" -> fn member -> member.active end
        "past" -> fn member -> !member.active end
      end

    name_constituency_f =
      case filter.name_constituency do
        "" ->
          fn _ -> true end

        _ ->
          case Regex.compile(filter.name_constituency, [:caseless]) do
            {:ok, rex} -> fn member -> Regex.match?(rex, member.name) end
            {:error, _} -> fn _ -> false end
          end
      end

    party_f = fn member ->
      short = Map.get(@party_mapping, member.party && member.party.name, "other")
      Map.get(filter.parties, short, false)
    end

    members
    |> Enum.filter(fn member ->
      active_f.(member) and
        (filter.house == "both" or
           filter.house == member.current_house) and
        name_constituency_f.(member) and
        party_f.(member)
    end)
  end
end
