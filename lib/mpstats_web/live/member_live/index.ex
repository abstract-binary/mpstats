defmodule MpstatsWeb.MemberLive.Index do
  use MpstatsWeb, :live_view

  alias Mpstats.Cache
  alias MpstatsWeb.MemberLive.Filter

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:members, Cache.get_members())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, params) do
    filter =
      Filter.changeset(%Filter{}, params) |> Ecto.Changeset.apply_changes() |> Map.from_struct()

    rows = Filter.apply(filter, socket.assigns.members)

    socket
    |> assign(:page_title, "MP Directory")
    |> assign(:filter, filter)
    |> assign(:rows, rows)
    |> assign(:len, length(rows))
    |> assign(:member, nil)
  end

  # Not used, left as example
  @impl true
  def handle_info({MpstatsWeb.MemberLive.FormComponent, {:saved, member}}, socket) do
    {:noreply, stream_insert(socket, :members, member)}
  end

  @impl true
  def handle_event(
        "filtersChanged",
        %{
          "housesToggles" => house,
          "nameConstituencyFilter" => text,
          "activeToggles" => active
        } = params,
        socket
      ) do
    parties = Map.get(params, "partyFilter", %{})

    filter =
      socket.assigns.filter
      |> put_in([:house], house)
      |> put_in([:name_constituency], text)
      |> put_in([:active], active)
      |> put_in(
        [:parties],
        parties |> Enum.map(fn {k, v} -> {k, v == "on"} end) |> Enum.into(%{})
      )

    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  def handle_event("first", _params, %{assigns: %{filter: filter}} = socket) do
    filter = put_in(filter, [:skip], 0)

    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  def handle_event("prev", _params, %{assigns: %{filter: filter}} = socket) do
    filter =
      put_in(filter, [:skip], max(0, filter.skip - filter.limit))

    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  def handle_event("next", _params, %{assigns: %{filter: filter} = assigns} = socket) do
    filter =
      put_in(filter, [:skip], min(assigns.len - filter.limit + 1, filter.skip + filter.limit))

    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  def handle_event("last", _params, %{assigns: %{filter: filter} = assigns} = socket) do
    filter =
      put_in(filter, [:skip], assigns.len - filter.limit + 1)

    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  defp select_rows(rows, %{filter: %{limit: limit, skip: skip}})
       when is_integer(limit) and is_integer(skip) do
    len = length(rows)

    cond do
      limit >= len ->
        rows

      skip >= len - 1 ->
        []

      true ->
        rows
        |> Enum.drop(skip)
        |> Enum.take(limit)
    end
  end
end
