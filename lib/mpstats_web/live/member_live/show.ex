defmodule MpstatsWeb.MemberLive.Show do
  use MpstatsWeb, :live_view

  alias Mpstats.Members

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    member =
      Members.get_member!(id)
      |> compact_house_memberships()
      |> compact_party_memberships()

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:member, member)
     |> assign(:votes_cast, Members.get_member_vote_results!(id))}
  end

  defp page_title(:show), do: "Show Member"

  defp parliament_link(%{id: id}) do
    "https://members.parliament.uk/member/#{id}/career"
  end

  defp compact_house_memberships(member) do
    house_memberships =
      member.house_memberships
      |> Enum.group_by(fn x -> {x.house, x.membership_from} end)
      |> Enum.map(fn {_, xs} -> merge_consecutive_dates(xs) end)
      |> Enum.sort_by(fn x -> x.start_date end, Date)

    %{member | house_memberships: house_memberships}
  end

  defp compact_party_memberships(member) do
    party_memberships =
      member.party_memberships
      |> Enum.group_by(fn x -> x.party.id end)
      |> Enum.map(fn {_, xs} -> merge_consecutive_dates(xs) end)
      |> Enum.sort_by(fn x -> x.start_date end, Date)

    %{member | party_memberships: party_memberships}
  end

  defp merge_consecutive_dates([%{start_date: _, end_date: _} = x | _] = xs) do
    start_date = xs |> Enum.map(& &1.start_date) |> Enum.sort(Date) |> List.first()

    end_date =
      xs
      |> Enum.map(& &1.end_date)
      |> Enum.sort(fn x, y ->
        case {x, y} do
          {_, nil} -> true
          {nil, _} -> false
          {x, y} -> Date.after?(y, x)
        end
      end)
      |> List.last()

    %{x | start_date: start_date, end_date: end_date}
  end
end
