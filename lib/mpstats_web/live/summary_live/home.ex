defmodule MpstatsWeb.SummaryLive.Home do
  use MpstatsWeb, :live_view

  alias Explorer.DataFrame, as: DF
  alias Mpstats.Cache

  @impl true
  def mount(_params, _session, socket) do
    members = Cache.get_members() |> Enum.filter(& &1.active) |> Enum.sort_by(& &1.party)
    parties = Cache.get_parties()

    {:ok,
     socket
     |> assign(:parties, parties)
     |> assign(:members, members)
     |> assign(:service_ages, service_ages(members))
     |> assign(:ages, ages(members))
     |> assign(
       :party_colors,
       parties |> Enum.map(fn {_, p} -> {p.name, p.color} end) |> Enum.unzip()
     )}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :home, _params) do
    socket
    |> assign(:page_title, "Home")
  end

  # Not used, left as example
  @impl true
  def handle_info({MpstatsWeb.MemberLive.FormComponent, {:saved, member}}, socket) do
    {:noreply, stream_insert(socket, :members, member)}
  end

  # Not used, left as an example
  @impl true
  def handle_event("filterToggled", _attrs, socket) do
    filter = update_in(socket.assigns.filter, [:active], &(!&1))
    {:noreply, socket |> push_patch(to: ~p"/members?#{filter}")}
  end

  defp service_ages(members) when is_list(members) do
    members
    |> Enum.map(
      &%{
        member_id: &1.id,
        house: &1.current_house,
        age: &1.service_age,
        party: &1.party.name
      }
    )
    |> DF.new()
  end

  defp ages(members) when is_list(members) do
    members
    |> Enum.map(
      &%{
        member_id: &1.id,
        house: &1.current_house,
        age: &1.age,
        party: &1.party.name
      }
    )
    |> DF.new()
  end
end
