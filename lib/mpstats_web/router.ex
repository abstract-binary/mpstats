defmodule MpstatsWeb.Router do
  use MpstatsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {MpstatsWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admins_only do
    plug :admin_basic_auth
  end

  scope "/", MpstatsWeb do
    pipe_through :browser

    live "/", SummaryLive.Home, :home

    live "/members", MemberLive.Index, :index
    live "/members/new", MemberLive.Index, :new
    live "/members/:id", MemberLive.Show, :show

    live "/exports", ExportsLive.Index, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", MpstatsWeb do
  #   pipe_through :api
  # end

  scope "/" do
    import Phoenix.LiveDashboard.Router
    pipe_through [:browser, :admins_only]

    live_dashboard "/dashboard",
      metrics: MpstatsWeb.Telemetry,
      additional_pages: [
        route_name: MpstatsWeb.Dashboard
      ]

    forward "/mailbox", Plug.Swoosh.MailboxPreview
    live "/problems", MpstatsWeb.ProblemsLive.Index, :index
  end

  defp admin_basic_auth(conn, _opts) do
    username = System.fetch_env!("ADMIN_USERNAME")
    password = System.fetch_env!("ADMIN_PASSWORD")
    Plug.BasicAuth.basic_auth(conn, username: username, password: password)
  end
end
