defmodule MpstatsWeb.Dashboard do
  @moduledoc """
  Live dashboard for MPStats.
  """

  # Examples:
  # https://hexdocs.pm/phoenix_live_dashboard/Phoenix.LiveDashboard.PageBuilder.html
  # https://github.com/evilmarty/oban_live_dashboard/blob/main/lib/oban/live_dashboard.ex

  use Phoenix.LiveDashboard.PageBuilder, refresher?: true

  import Phoenix.LiveDashboard.Helpers, only: [format_value: 2]
  import Ecto.Query

  require Logger

  @impl true
  def menu_link(_, _) do
    {:ok, "MPStats"}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <h2>Oban</h2>
    <table class="table table-hover dash-table">
      <thead>
        <tr>
          <th>Schedule</th>
          <th>Queue</th>
          <th>Params</th>
        </tr>
      </thead>
      <tbody>
        <tr :for={
          {schedule, queue, params} <-
            Keyword.get(Oban.config().plugins, Oban.Plugins.Cron, [])
            |> Keyword.get(:crontab, [])
        }>
          <td><%= schedule %></td>
          <td><%= queue %></td>
          <td><%= inspect(params) %></td>
        </tr>
      </tbody>
    </table>
    <.live_table
      id="jobs-table"
      dom_id="jobs-table"
      page={@page}
      title="Jobs"
      limit={[5, 10, 50, 100]}
      row_fetcher={&fetch_jobs/2}
      row_attrs={&row_attrs/1}
      rows_name="tables"
      search={false}
    >
      <:col field={:id} header="Id" sortable={:desc} />
      <:col field={:state} header="State" sortable={:desc} />
      <:col :let={job} field={:args} header="Args" sortable={:desc}>
        <%= format_value(job.args) %>
      </:col>
      <:col :let={job} field={:inserted_at} sortable={:desc}>
        <%= format_value(job.inserted_at) %>
      </:col>
      <:col :let={job} field={:scheduled_at} sortable={:desc}>
        <%= format_value(job.scheduled_at) %>
      </:col>
      <:col :let={job} field={:ended_at} sortable={:desc}>
        <%= format_value(job.completed_at || job.cancelled_at || job.discarded_at) %>
      </:col>
    </.live_table>
    <.live_modal
      :if={@job != nil}
      id="job-info"
      title="Job"
      return_to={live_dashboard_path(@socket, @page, params: %{})}
    >
      <.label_value_list>
        <:elem label="Id"><%= @job.id %></:elem>
        <:elem label="State"><%= @job.state %></:elem>
        <:elem label="Queue"><%= @job.queue %></:elem>
        <:elem label="Worker"><%= @job.worker %></:elem>
        <:elem label="Args"><%= format_value(@job.args, nil) %></:elem>
        <:elem :if={@job.meta != %{}} label="Meta"><%= format_value(@job.meta, nil) %></:elem>
        <:elem :if={@job.tags != []} label="Tags"><%= format_value(@job.tags, nil) %></:elem>
        <:elem :if={@job.errors != []} label="Errors"><%= format_errors(@job.errors) %></:elem>
        <:elem label="Attempts"><%= @job.attempt %>/<%= @job.max_attempts %></:elem>
        <:elem label="Priority"><%= @job.priority %></:elem>
        <:elem label="Attempted at"><%= format_value(@job.attempted_at) %></:elem>
        <:elem label="Cancelled at"><%= format_value(@job.cancelled_at) %></:elem>
        <:elem label="Completed at"><%= format_value(@job.completed_at) %></:elem>
        <:elem label="Discarded at"><%= format_value(@job.discarded_at) %></:elem>
        <:elem label="Inserted at"><%= format_value(@job.inserted_at) %></:elem>
        <:elem label="Scheduled at"><%= format_value(@job.scheduled_at) %></:elem>
      </.label_value_list>
    </.live_modal>
    """
  end

  @impl true
  def handle_params(%{"params" => %{"job" => job_id}}, _url, socket) do
    case fetch_job(job_id) do
      {:ok, job} ->
        {:noreply, assign(socket, job: job)}

      :error ->
        to = live_dashboard_path(socket, socket.assigns.page, params: %{})
        {:noreply, push_patch(socket, to: to)}
    end
  end

  def handle_params(_params, _url, socket) do
    {:noreply, assign(socket, job: nil)}
  end

  @impl true
  def handle_event("show_job", params, socket) do
    Logger.info(socket.assigns)
    Logger.error(live_dashboard_path(socket, socket.assigns.page, params: params))
    to = live_dashboard_path(socket, socket.assigns.page, params: params)
    {:noreply, push_patch(socket, to: to)}
  end

  defp fetch_jobs(params, _node) do
    %{search: _search, sort_by: sort_by, sort_dir: sort_dir, limit: limit} = params
    total_jobs = Oban.Repo.aggregate(Oban.config(), Oban.Job, :count)

    jobs =
      Oban.Repo.all(
        Oban.config(),
        Oban.Job
        |> limit(^limit)
        |> order_by({^sort_dir, ^sort_by})
      )
      |> Enum.map(&Map.from_struct/1)

    {jobs, total_jobs}
  end

  defp row_attrs(job) do
    [
      {"phx-click", "show_job"},
      {"phx-value-job", job[:id]},
      {"phx-page-loading", true}
    ]
  end

  defp fetch_job(job_id) do
    case Oban.Repo.get(Oban.config(), Oban.Job, job_id) do
      nil -> :error
      job -> {:ok, job}
    end
  end

  defp format_errors(errors) do
    Enum.map(errors, &Map.get(&1, "error"))
  end

  defp format_value(%DateTime{} = datetime) do
    if DateTime.to_date(datetime) == Date.utc_today() do
      Calendar.strftime(datetime, "%H:%M:%S")
    else
      Calendar.strftime(datetime, "%Y-%m-%d %H:%M:%S")
    end
  end

  defp format_value(x), do: inspect(x)
end
