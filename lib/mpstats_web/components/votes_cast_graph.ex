defmodule MpstatsWeb.Live.VotesCastGraph do
  use MpstatsWeb, :live_component

  @impl true
  def update(assigns, socket) do
    spec =
      VegaLite.new(title: "Votes Cast", width: 400, height: 300)
      |> VegaLite.data_from_values(assigns.data, only: ["date", "vote"])
      |> VegaLite.mark(:bar, tooltip: true)
      |> VegaLite.encode_field(:x, "date", type: :temporal, time_unit: "yearmonth")
      |> VegaLite.encode(:y, aggregate: :count)
      |> VegaLite.encode_field(:color, "vote", type: :nominal)
      |> VegaLite.to_spec()

    {:ok,
     socket
     |> assign(:id, assigns.id)
     |> push_event("vega_lite:#{assigns.id}:init", %{"spec" => spec})}
  end

  @impl true
  def render(assigns) do
    # Here we have the element that will load the embedded
    # view. Special note to `data-id` which is the identifier that
    # will be used by the hooks to understand which socket sent want.

    # We also identify the hook that will use this component using
    # phx-hook.  Refer again to
    # https://hexdocs.pm/phoenix_live_view/js-interop.html#client-hooks-via-phx-hook
    ~H"""
    <div id={@id} phx-hook="VegaLite" phx-update="ignore" data-id={@id} />
    """
  end
end
