defmodule MpstatsWeb.NavComponents do
  @moduledoc """
  Provides components used in the `<nav>` section at the top of every
  page.
  """
  use Phoenix.Component
  alias MpstatsWeb.CoreComponents, as: CC

  @doc """
  Renders a nav link for wide displays.

  ## Examples

    <.nav_link_full href={~p"/"} title="Home" page_title={@page_title} />
  """

  attr :href, :string, required: true
  attr :title, :string, required: true
  attr :page_title, :string, required: true
  attr :icon, :string, default: nil

  def nav_link_full(assigns) do
    # Common: group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold
    # Active: bg-gray-50 text-indigo-600
    # Inactive: text-gray-700 hover:text-indigo-600 hover:bg-gray-50
    ~H"""
    <a
      href={@href}
      class={[
        "group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold",
        @page_title == @title && "bg-gray-50 text-indigo-600",
        @page_title != @title &&
          "text-gray-700 hover:text-indigo-600 hover:bg-gray-50"
      ]}
      aria-current={if @page_title == @title, do: "page", else: nil}
    >
      <%= if @icon do %>
        <CC.icon name={@icon} />
      <% end %>
      <%= @title %>
    </a>
    """
  end

  @doc """
  Renders a nav link for mobile.

  ## Examples

    <.nav_link_mobile href={~p"/"} title="Home" page_title={@page_title} />
  """

  attr :href, :string, required: true
  attr :title, :string, required: true
  attr :page_title, :string, required: true

  def nav_link_mobile(assigns) do
    # Current: "border-indigo-500 bg-indigo-50 text-indigo-700", Default: "border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800"
    ~H"""
    <a
      href={@href}
      class={[
        "block border-l-4 py-2 pl-3 pr-4 text-base font-medium",
        @page_title == @title && "border-indigo-500 bg-indigo-50 text-indigo-700",
        @page_title != @title &&
          "border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800"
      ]}
      aria-current={if @page_title == @title, do: "page", else: nil}
    >
      <%= @title %>
    </a>
    """
  end
end
