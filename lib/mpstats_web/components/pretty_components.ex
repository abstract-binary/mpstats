defmodule MpstatsWeb.PrettyComponents do
  use Phoenix.Component

  alias MpstatsWeb.CoreComponents, as: CC

  @doc """
  Renders a heading at the given level.

  ## Examples

  ```
  <.heading level={2} ref="ref">Title text</.heading>
  ```
  """
  attr :level, :integer, default: 2
  attr :ref, :string, required: true
  slot :inner_block, required: true
  slot :subtitle

  def heading(%{level: level} = assigns) do
    case level do
      2 ->
        ~H"""
        <div class="border-b border-gray-200 mt-4 group">
          <h2 class="text-2xl font-semibold leading-6 text-gray-900">
            <a href={"##{@ref}"}>
              <%= render_slot(@inner_block) %>
              <CC.icon name="hero-link" class="text-transparent group-hover:text-slate-700" />
            </a>
          </h2>
          <p :if={@subtitle} class="mt-2 max-w-4xl text-base text-gray-500">
            <%= render_slot(@subtitle) %>
          </p>
        </div>
        """

      3 ->
        ~H"""
        <h3 class="text-base font-semibold leading-6 text-gray-900 group">
          <a href={"##{@ref}"}>
            <%= render_slot(@inner_block) %>
            <CC.icon name="hero-link" class="text-transparent group-hover:text-slate-700" />
          </a>
        </h3>
        """

      _ ->
        ~H"""
        <h6>Unknown heading level <%= @level %>: <%= render_slot(@inner_block) %></h6>
        """
    end
  end

  @doc """
  Renders a toggle checkbox.
  """
  attr :on, :boolean, required: true
  attr :event, :string, required: true
  slot :inner_block
  slot :subtitle

  def toggle(assigns) do
    # <.form :let={f} for={@filter} as={:filter} phx-change="filterChanged">
    #   <.input field={f[:active]} label="Active only" type="checkbox" />
    # </.form>

    ~H"""
    <div>
      <label class="flex items-center justify-between">
        <span class="flex flex-grow flex-col">
          <span class="text-base font-medium leading-6 text-gray-900" id="availability-label">
            <%= render_slot(@inner_block) %>
          </span>
          <span :if={@subtitle} class="text-sm text-gray-500" id="availability-description">
            <%= render_slot(@subtitle) %>
          </span>
        </span>
        <button
          type="button"
          class={[
            "relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-indigo-600 focus:ring-offset-2",
            @on && "bg-indigo-600",
            !@on && "bg-gray-200"
          ]}
          role="switch"
          aria-checked={@on}
          aria-labelledby="availability-label"
          aria-describedby="availability-description"
          phx-click={@event}
        >
          <span
            aria-hidden="true"
            class={[
              "translate-x-0 pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out",
              @on && "translate-x-5",
              !@on && "translate-x-0"
            ]}
          >
          </span>
        </button>
      </label>
    </div>
    """
  end

  @doc """
  Renders a group of radio boxes.

  ## Examples

  ```
  <.radio_boxes checked={@filter.active} name="activeToggles">
    Active
    <:subtitle>Show only currently active or all MPs</:subtitle>
    <:option name="all">All</:option>
    <:option name="active">Active</:option>
    <:option name="past">Past</:option>
  </.radio_boxes>
  ```
  """
  slot :inner_block
  slot :subtitle
  attr :checked, :string, required: true
  attr :name, :string, required: true

  slot :option do
    attr :name, :string, required: true
  end

  def radio_boxes(assigns) do
    ~H"""
    <div>
      <label class="text-base font-semibold text-gray-900"><%= render_slot(@inner_block) %></label>
      <p :if={@subtitle} class="text-sm text-gray-500"><%= render_slot(@subtitle) %></p>
      <fieldset class="mt-2">
        <legend class="sr-only"><%= render_slot(@inner_block) %></legend>
        <div class="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0">
          <%= for o <- @option do %>
            <div class="flex items-center">
              <input
                id={o.name}
                name={@name}
                type="radio"
                checked={@checked == o.name}
                class="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600"
                phx-value-radio={o.name}
                value={o.name}
              />
              <label for={o.name} class="ml-3 block text-sm font-medium leading-6 text-gray-900">
                <%= render_slot(o) %>
              </label>
            </div>
          <% end %>
        </div>
      </fieldset>
    </div>
    """
  end

  @doc """
  Renders a group of check boxes.

  ## Examples

  ```
  <.check_boxes>
    Parties
    <:subtitle>Show MPs only from the selected parties</:subtitle>
    <:option>Tories</:option>
    <:option>Labour</:option>
    <:option>LibDems</:option>
    <:option>SNP</:option>
  </.check_boxes
  ```
  """
  slot :inner_block
  slot :subtitle
  attr :checked, :any, required: true
  attr :name, :string, required: true

  slot :option do
    attr :name, :string, required: true
  end

  def check_boxes(assigns) do
    ~H"""
    <div>
      <label class="text-base font-semibold text-gray-900"><%= render_slot(@inner_block) %></label>
      <p :if={@subtitle} class="text-sm text-gray-500"><%= render_slot(@subtitle) %></p>
      <fieldset class="m-2">
        <legend class="sr-only"><%= render_slot(@inner_block) %></legend>
        <div class="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0">
          <%= for o <- @option do %>
            <label class="relative flex items-start">
              <div class="flex h-6 items-center">
                <input
                  id={o.name}
                  name={"#{@name}[#{o.name}]"}
                  type="checkbox"
                  class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                  phx-value-checkbox={o.name}
                  checked={@checked[o.name]}
                />
              </div>
              <div class="ml-3 text-sm leading-6">
                <span class="font-medium text-gray-900"><%= render_slot(o) %></span>
              </div>
            </label>
          <% end %>
        </div>
      </fieldset>
    </div>
    """
  end

  @doc """
  Render an MP card as two lines and a picture.
  """

  attr :member, :any, required: true

  def member_two_line(assigns) do
    ~H"""
    <div class="flex items-center">
      <div class="h-11 w-11 flex-shrink-0">
        <%= if @member.thumbnail_url do %>
          <img
            class={["h-11 w-11 rounded-full border-2", "border-[#{@member.party.color}]"]}
            src={@member.thumbnail_url}
            alt=""
          />
        <% else %>
          <img class={["h-11 w-11 rounded-full border-2", "border-[#{@member.party.color}]"]} alt="" />
        <% end %>
      </div>
      <div class="ml-4">
        <div class="font-medium text-gray-900"><%= @member.name %></div>
        <div class="mt-1 text-gray-500"><%= @member.current_membership_from %></div>
      </div>
    </div>
    """
  end

  @doc """
  Render a text input used for filtering.
  """

  slot :inner_block
  attr :name, :string, required: true
  attr :value, :string, required: true

  def filter_box(assigns) do
    ~H"""
    <div>
      <label>
        <div class="text-base font-medium leading-6 text-gray-900">
          <%= render_slot(@inner_block) %>
        </div>
        <div class="relative mt-2 flex items-center">
          <input
            type="text"
            name={@name}
            class="block w-full rounded-md border-0 py-1.5 pr-14 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            value={@value}
          />
        </div>
      </label>
    </div>
    """
  end

  @doc """
  Renders a logo or a light-grey rectangle.

  ## Examples

      <.logo abbrev="LD" alt="Liberal Democrat" height={40} />
      <.logo abbrev="Con" alt="Conservative" height={40} />
      <.logo abbrev="Lab" alt="Labour" height={40} />
      <.logo abbrev="SNP" alt="Scottish National Party" height={40} />
  """
  attr :abbrev, :string, required: true
  attr :alt, :string, required: true
  attr :height, :integer, required: true
  attr :class, :string, default: ""

  def logo(%{abbrev: _, alt: _, height: _, class: _} = assigns) do
    ~H"""
    <div class={@class}>
      <%= Phoenix.HTML.raw(Mpstats.Logos.svg(@abbrev, @height, fallback: @alt)) %>
    </div>
    """
  end
end
