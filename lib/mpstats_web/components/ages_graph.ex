defmodule MpstatsWeb.Live.AgesGraph do
  use MpstatsWeb, :live_component

  alias VegaLite, as: Vl

  @impl true
  def update(assigns, socket) do
    spec =
      Vl.new(title: assigns.title, width: 400, height: 300)
      |> Vl.data_from_values(assigns.data)
      |> Vl.transform(filter: assigns.filter)
      |> Vl.mark(:bar, tooltip: true, orient: :horizontal)
      |> Vl.encode_field(:y, "age",
        type: :quantitative,
        title: "Age",
        bin: [step: 5],
        scale: [domain: Map.get(assigns, :domain, [0, 65])]
      )
      |> Vl.encode_field(:x, "count", aggregate: :count, title: "# Members")
      |> Vl.encode_field(:color, "party",
        type: :nominal,
        legend: nil,
        scale: [
          domain: elem(assigns.party_colors, 0),
          range: elem(assigns.party_colors, 1)
        ]
      )
      |> Vl.to_spec()

    {:ok,
     socket
     |> assign(:id, assigns.id)
     |> push_event("vega_lite:#{assigns.id}:init", %{"spec" => spec})}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div id={@id} phx-hook="VegaLite" phx-update="ignore" data-id={@id} />
    """
  end
end
