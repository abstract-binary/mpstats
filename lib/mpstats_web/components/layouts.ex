defmodule MpstatsWeb.Layouts do
  use MpstatsWeb, :html

  embed_templates "layouts/*"
end
