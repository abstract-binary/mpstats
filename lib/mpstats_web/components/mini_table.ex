defmodule MpstatsWeb.Live.MiniTable do
  @moduledoc """
  A miniature table with a few sortable columns.
  """

  use MpstatsWeb, :live_component

  @doc """
  Render a miniature table.
  """

  attr :id, :string, required: true
  attr :data, :any, required: true
  attr :filter, :any
  attr :sort, :any

  slot :index do
    attr :title, :string, required: true
  end

  slot :col do
    attr :title, :string, required: true
    attr :get, :any, required: true
  end

  def live(assigns) do
    ~H"""
    <.live_component
      module={MpstatsWeb.Live.MiniTable}
      id={@id}
      data={@data}
      filter={@filter}
      index={@index}
      col={@col}
    />
    """
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div class="flow-root">
      <div class="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <table id={@id} class="min-w-full divide-y divide-gray-300">
            <thead>
              <tr>
                <th
                  :for={index <- @index}
                  scope="col"
                  class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                >
                  <%= index.title %>
                </th>
                <%= for {col, idx} <- Enum.with_index(@col, 0) do %>
                  <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                    <a
                      href="#"
                      class="group inline-flex"
                      phx-click="sort"
                      phx-target={@myself}
                      phx-value-idx={idx}
                    >
                      <%= col.title %>
                      <%= case @sort do %>
                        <% {:desc, ^idx} -> %>
                          <span class="ml-2 flex-none rounded bg-gray-200 text-blue-900 group-hover:bg-gray-300">
                            <.icon name="hero-chevron-down" />
                          </span>
                        <% {:asc, ^idx} -> %>
                          <span class="ml-2 flex-none rounded bg-gray-200 text-blue-900 group-hover:bg-gray-300">
                            <.icon name="hero-chevron-up" />
                          </span>
                        <% _ -> %>
                          <span class="ml-2 flex-none rounded bg-gray-100 text-gray-900 group-hover:bg-gray-200">
                            <.icon name="hero-chevron-down" />
                          </span>
                      <% end %>
                    </a>
                  </th>
                <% end %>
              </tr>
            </thead>
            <tbody class="divide-y divide-gray-200">
              <%= for row <- select_rows(@rows, assigns) do %>
                <tr phx-click={JS.navigate(~p"/members/#{row}")} class="hover:cursor-pointer">
                  <td class="whitespace-nowrap px-3 py-2">
                    <%= render_slot(@index, row) %>
                  </td>
                  <td :for={col <- @col} class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                    <%= col.get.(row) %>
                  </td>
                </tr>
              <% end %>
            </tbody>
          </table>
          <div class="grid place-items-center grid-cols-1">
            <div>Page <%= floor(1 + @skip / @limit) %> of <%= ceil(@len / @limit) %></div>
            <nav class="isolate inline-flex -space-x-px rounded-md shadow-sm" aria-label="Pagination">
              <a
                href="#"
                class="relative inline-flex items-center rounded-l-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0"
                phx-click="first"
                phx-target={@myself}
              >
                <span class="sr-only">First</span>
                <.icon name="hero-chevron-double-left" />
              </a>
              <a
                href="#"
                class="relative hidden items-center px-4 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 md:inline-flex"
                phx-click="prev"
                phx-target={@myself}
              >
                <span class="sr-only">Previous</span>
                <.icon name="hero-chevron-left" />
              </a>
              <a
                href="#"
                class="relative hidden items-center px-4 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 md:inline-flex"
                phx-click="next"
                phx-target={@myself}
              >
                <span class="sr-only">Next</span>
                <.icon name="hero-chevron-right" />
              </a>
              <a
                href="#"
                class="relative inline-flex items-center rounded-r-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0"
                phx-click="last"
                phx-target={@myself}
              >
                <span class="sr-only">End</span>
                <.icon name="hero-chevron-double-right" />
              </a>
            </nav>
          </div>
        </div>
      </div>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    rows = assigns.data |> Enum.filter(fn row -> assigns.filter.(row) end)
    sort = get_in(assigns, [:sort]) || {:desc, 0}

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:rows, rows)
     |> assign(:limit, 5)
     |> assign(:skip, 0)
     |> assign(:sort, sort)
     |> assign(:len, length(rows))
     |> apply_sort()}
  end

  @impl true
  def handle_event("first", _params, socket) do
    {:noreply, socket |> assign(:skip, 0)}
  end

  def handle_event("prev", _params, socket) do
    {:noreply,
     socket
     |> assign(:skip, max(0, socket.assigns.skip - socket.assigns.limit))}
  end

  def handle_event("next", _params, socket) do
    {:noreply,
     socket
     |> assign(
       :skip,
       min(
         socket.assigns.len - socket.assigns.limit + 1,
         socket.assigns.skip + socket.assigns.limit
       )
     )}
  end

  def handle_event("last", _params, socket) do
    {:noreply,
     socket
     |> assign(:skip, socket.assigns.len - socket.assigns.limit + 1)}
  end

  def handle_event("sort", %{"idx" => idx}, socket) do
    idx = String.to_integer(idx)

    sort =
      case socket.assigns.sort do
        {:asc, ^idx} -> {:desc, idx}
        {:desc, ^idx} -> {:asc, idx}
        _ -> {:desc, idx}
      end

    {:noreply, socket |> assign(:sort, sort) |> apply_sort()}
  end

  defp select_rows(rows, %{limit: limit, skip: skip, len: len})
       when is_integer(limit) and is_integer(skip) and is_integer(len) do
    cond do
      limit >= len ->
        rows

      skip >= len - 1 ->
        []

      true ->
        rows
        |> Enum.drop(skip)
        |> Enum.take(limit)
    end
  end

  defp apply_sort(socket) do
    rows = sort_rows(socket.assigns.rows, socket.assigns)

    socket
    |> assign(rows: rows)
  end

  defp sort_rows(rows, %{col: col, sort: {order, idx}})
       when order in [:asc, :desc] and is_integer(idx) do
    get = Enum.at(col, idx).get

    rows
    |> Enum.sort_by(get, order)
  end
end
