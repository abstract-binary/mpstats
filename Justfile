default:
	just --choose

# Start a dev IEx shell with the application loaded
dev:
	# mix deps.get
	# mix deps.compile
	iex --sname phoenix -S mix phx.server

# Open an IEx shell into the prod instance (TAKE CARE WITH HOW YOU DC)
prod-remote-shell:
	# fly ssh issue --agent
	TERM=xterm-256color fly ssh console --pty --select -C "/app/bin/mpstats remote"

# Open a psql into the local dev database
psql:
	psql -d mpstats_dev

# Deploy the app to prod
prod-deploy:
	fly deploy

# Connect to the prod database
prod-psql:
	fly postgres connect -a mpstats-db -d mpstats
